var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GroupSchema = new Schema({
	name:{
		type:String,
		required:true
	},
	description:{
		type: String,
		required:true
	},
	members:[{
		type:Schema.Types.ObjectId,
		ref:'member'
	}],
	website:{
		type:Schema.Types.ObjectId,
		ref:'website'
	}

});

const Group = mongoose.model('group',GroupSchema);
module.exports = Group;

