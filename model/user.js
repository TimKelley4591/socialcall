const mongoose = require('mongoose');
const AddressSchema = require('./addressSchema');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	firstName:{
		type:String,
		required:false
	},
	lastName:{
		type:String,
		required:false
	},
	phoneNumber:{
		type:String,
		required:false
	},
	email:{
		type:String,
		required:false
	},
	address:AddressSchema,
	billingAddress:AddressSchema,
	groups:[{
		type:Schema.Types.ObjectId,
		ref:'group'
	}
	]

});

const User = mongoose.model('user',UserSchema);
module.exports = User;
