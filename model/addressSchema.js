var mongoose = require('mongoose');
const Schema = mongoose.Schema;


const AddressSchema = new Schema({
	companyName:{
		type: String,
		required:false
	},
	streetAddress:{
		type: String,
		required:true
	},
	stateProvince:{
		type:String,
		required:true
	},
	zip:{
		type:String,
		required:true
	},
	country:{
		type: String,
		required: true
	}
});

module.export = AddressSchema;

