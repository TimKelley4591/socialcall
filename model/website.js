var mongoose = require('mongoose');
const Schema = mongoose.Schema;
:


const WebsiteSchema = new Schema({
	name:{
		type:String,
		required:true
	},
	description:{
		type: String,
		required:true
	},
	domain:{
		type:String,
		required:false
	}
});

const Website = mongoose.model('website',WebsiteSchema);

module.exports = Website;

