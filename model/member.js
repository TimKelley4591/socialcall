const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AddressSchema = require('./addressSchema');

const MemberSchema = new Schema({
	firstName:{
		type:String,
		required:false
	},
	lastName:{
		type:String,
		required:false
	},
	phoneNumber:{
		type:String,
		required:false
	},
	email:{
		type:String,
		required:false
	},
	text:{
		type:Boolean,
		required:false
	},
	active:{
		type:Boolean,
		default:true
	},
	address:{
		type:AddressSchema,
		required:false
	}});

const Member = mongoose.model('member',MemberSchema);
module.exports = Member;
