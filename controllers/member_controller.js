const Member = require('../model/member');
module.exports = {
	get(req,res){
		const groupId = req.params.groupId;
		Member.find({groupId:groupId})
			.then(members =>res.status(200).send(members))
		.catch(next);
	},
	create(req,res,next){
		const memberProps = req.body;
		Member.create(memberProps)
			.then((member) => {
				res.send(member);
			})
			.catch(next);
	},
	edit(req,res,next){
		const memberId = req.params.id
		const memberProps = req.body;
		Member.findByIdAndUpdate({_id:memberId},memberProps)
		.then(()=> Member.findById({_id:memberId}))
		.then(member => res.send(member))
		.catch(next);
	},

	delete(req,res,next){
		const memberId = req.params.id;
		Member.findByIdAndRemove({_id:memberId})
			.then(member => res.status(204).send(member))
		.catch(next);
	}


};
