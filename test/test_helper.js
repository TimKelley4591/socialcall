const mongoose = require('mongoose');

mongoose.promise = global.Promise;
before(done => {
mongoose.connect('mongodb://localhost/socialCallunittests');
mongoose.connection
	.once('open',()=> done())
	.on('error',err =>{
		console.warn('Warning',err);
	});
});

beforeEach((done)=> {
	const users = mongoose.connection.collections.users;
	const groups = mongoose.connection.collections.groups;
	const members = mongoose.connection.collections.members;
		users.drop(()=>{
			groups.drop(()=>{
				members.drop(()=>{
					done();
			});
		});
	});
});


