const request = require('supertest');
const app = require('../../app');
const assert = require('assert');
const mongoose = require('mongoose');
const User = mongoose.model('user');

describe('Users Controller', () => {

	it('GET to /api/users  get all users',(done) => {
		const user1 = new User({email:'abelwebsolutions@gmail.com'});
		const user2 = new User({email:'5kelleys@gmail.com'});

		Promise.all([user1.save(),user2.save()]).then((results)=> {
			request(app)
				.get(`/api/users/`)
				.end((err,response)=> {
				           User.count().then(count => {
						   assert(count === 2);
				                  done();
					     });
				});
		});
	});

	it('GET to /api/user get an exiting user',(done) => {
		const user = new User({email:'abelwebsolutions@gmail.com'});
		user.save().then((newUser) =>
		{
		request(app)
			.get(`/api/users/${newUser._id}`)
			.end((err,response) => {
			assert(response.body.email === 'abelwebsolutions@gmail.com');
			done();
			});
		});
	});
	
	it('Post to /api/user create a new user', (done) => {
		request(app)
			.post('/api/users')
			.send({email:'test@test.com'})
				.end((err,response) => {
					User.findOne({email:'test@test.com'}).then((newUser)=> {
						assert(newUser.email === 'test@test.com');
						done();
				});
			});
	});

	it('PUT to /api/users/id edits an existing user', (done) =>{
		const user = User.create = new User({email:'abelwebsolutions@gmail.com'});
		user.save().then((user) =>{
			request(app)
			.put(`/api/users/${user._id}`)
			.send({email:'5kelleys@gmail.com'})
			.end(()=> {
				User.findOne({_id:user._id})
					.then(updatedUser => {
						assert(updatedUser.email === '5kelleys@gmail.com');
						done();
					});
			});

		});

	});

	it('Delete to /api/user/:id can delete a record', done => {
		    const user  = new User({ email: 'test@test.com' });
		    user.save().then(() => {
			          request(app)
			            .delete(`/api/users/${user._id}`)
			            .end(() => {
				           User.count().then(count => {
						   assert(count === 0);
				                  done();
					     });
				        });
			 });
	  });
});
