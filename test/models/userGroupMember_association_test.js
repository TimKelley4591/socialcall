'use strict'
const assert = require('assert');
const mongoose = require('mongoose');
const User = require('../../model/user');
const Member = require('../../model/member'); 
const Group = require('../../model/group');

describe('Associations', ()=> {
	        let user,newGroup,newMember,newMember2;

	beforeEach((done) => {
		user = new User({
			firstName:'Donna',
			lastName:'Kelley',
			email:'5kelleys@gmail.com',
			phone:'7068329632',
			address:{
			companyName:'Enterprise Software',
				streetAddress:'6748 Washington Road',
				city:'Appling',
				state:'Ga',
				zip:'30802'
			},
			billingAddress:{
				companyName:'Enterprise Software',
				streetAddress:'6748 Washington Road',
				city:'Appling',
				state:'Ga',
				zip:'30802'
			}
			
		});
		
		newGroup = new Group({
			name:'The Band Kelley',
			description:'Family Band'
		});

		newMember  = new Member({
				firstName:'Tim',
				lastName:'Kelley',
			email:'abelwebsolutions@gmail.com',
			phoneNumber:'7068329632'
		});

		user.groups.push(newGroup);

		 newMember2  = new Member({
			                                 firstName:'Tim',
			                                 lastName:'Kelley',
			                         email:'abelwebsolutions@gmail.com',
			                         phoneNumber:'7068329632'
			                 });
		newGroup.members.push(newMember);
		newGroup.members.push(newMember2);

		
				 Promise.all([user.save(),newGroup.save(),newMember.save()])
						.then(() => done());
	});

	it('test assocation of user and group and members only populated with ids',function(done){
		User.findOne({firstName:'Donna'})
			.populate({
				path:'groups',
				model:'group'
			})
			.then((user) => {
				assert(user.firstName === 'Donna');
				assert(user.groups[0].name === 'The Band Kelley');
				assert(user.groups[0].description === 'Family Band');
				assert(user.groups[0].members.length  === 2);

			done();
		});
	
	});

	it('test association of group and new member',function(done){
		User.findOne({firstName:'Donna'})
			.populate({
				path:'groups',
				populate:{
					path:'members',
					model:'member'
				}

			})
			.then((user)=> {
				assert(user.groups[0].members[0].firstName === 'Tim' );
				assert(user.groups[0].members[0].lastName === 'Kelley');
				assert(user.groups[0].members[0].email  === 'abelwebsolutions@gmail.com');
				assert(user.groups[0].members[0].phoneNumber === '7068329632');

				done();
			});
	});

});
