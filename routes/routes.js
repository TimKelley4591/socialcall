
const MemberController = require('../controllers/member_controller');
const UserController = require('../controllers/user_controller');


module.exports = (app)=> {
	app.get('/api/users',UserController.getAllUsers);
	app.get('/api/users/:id',UserController.get);
	app.post('/api/users',UserController.create);
	app.put('/api/users/:id',UserController.edit);
	app.delete('/api/users/:id',UserController.delete);

};


